class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.float :price
      t.string :p_year
      t.string :category
      t.string :r_status
      t.string :r_user

      t.timestamps
    end
  end
end
