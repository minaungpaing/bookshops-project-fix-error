class AdminController < ApplicationController
  skip_before_action :require_login,only:[:new,:create]
  def new
  end

  def create
  	admin = Admin.find_by_name(params[:name])
  	if admin && admin.authenticate(params[:password])
  		session[:admin_id] = admin.id
  		redirect_to admin_login_url, notice: "Welcome to admin!"
  	else
  		flash.now[:alert] = "Name or password is invalid!"
  		render "new"
  	end
  end

  def index
  	@book = Book.all
  end

  def destroy
  	session[:admin_id] = nil
  	redirect_to root_url,notice: "Admin Logged Out!"
  end
end
