class BookController < ApplicationController
  skip_before_action :require_login, only: [:new]
  def new
  	
  end
  def create
    	book = Book.new(book_params)
  	if book.save
  		redirect_to admin_login_url 
  	else
  		flash[:errors] = book.errors.full_messages
  		redirect_to "/book/add"	
  	end
  end

  def edit
  	@book = Book.find(params[:id])
  end

  def destroy
  	book = Book.find(params[:id])
	book.destroy
	redirect_to admin_login_url
  end
  def r_user
  	@user = Book.where(r_status:"yes")
  end

  def update
    book = Book.find(params[:id])
    if book.update(book_params)
      redirect_to admin_login_url
    else
    flash.now[:alert] = "Update Error!"
    redirect_to admin_login_url 
    end 
  end

  private
  	def book_params
  		params.require(:book).permit(:title,:author,:p_year,:price,:category,:r_status,:img)
  	end
end
