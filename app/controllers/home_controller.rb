class HomeController < ApplicationController
  skip_before_action :require_login, only:[:index,:reserve,:cancel,:r_list,:search]
  def index
  	@books = Book.all
  end

  def reserve
  	book = Book.find(params[:id])
  	if book.update(r_status:"yes",r_user: current_user.name)
		redirect_to root_url
	else
		flash.now[:alert] = "Error!"
		redirect_to root_url	
	end
  end
  def cancel
 	book = Book.find (params[:id])
   	if book.update(r_status:"no",r_user: " ")
   		redirect_to root_url
   	else
  		flash.now[:alert] = "Error!"
  		redirect_to root_url	
   	end
  end

  def r_list
    @list = Book.where(r_user:current_user.name)
  end

  def search
    @books = Book.where("category LIKE ?","%" + params[:search] + "%")
  end
end
