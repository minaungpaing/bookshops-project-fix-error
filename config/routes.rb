Rails.application.routes.draw do
  
  get 'book/new'
  get 'book/edit'
  get 'book/destroy'
  get 'admin/new'
  get 'admin/create'
  get 'admin/destroy'
   get 'home/index'
   root 'home#index'
  # get 'admin/index'
  # get 'admin/destory'
  # get 'sessions/new'
  # get 'sessions/create'
  # get 'sessions/destroy'
  resources :users
  resources :sessions, only:[:new,:create,:destroy]
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  get 'reserve/:id', to: 'home#reserve'
  get 'cancel/:id', to: 'home#cancel'

  resources :admin, only:[:new ,:create]
  get 'admin_logout', to: 'admin#destroy', as: 'admin_logout'
  get 'admin', to: 'admin#new', as: 'admin'
  get 'admin_login', to: 'admin#index', as: 'admin_login'

  get '/book/add', to: 'book#new'
  get '/book/create', to: 'book#create'
  get '/book/edit/:id', to: 'book#edit'
  delete '/book/:id', to: 'book#destroy'

  get '/book/r_user', to: 'book#r_user'
  get '/r_list', to: 'home#r_list'
  get '/search', to: 'home#search' 

  post 'update/:id', to: 'book#update'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
